const merge = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
  output: {
    path: path.resolve('dist'),
    filename: '[name]-[hash].js',
  },
  plugins: [
    new UglifyJsPlugin(),
  ],
});
