import React from 'react';

export default () => (
  <section className="contact pure-g">
    <div className="container">
      <h2 className="pure-u-1 is-center section-title">
        <i className="fa fa-users" />
        Contact Us
      </h2>

      <p className="pure-u-1 is-center contact-text">
        Do you wanna have your university included here? <br />
        Send us the website that shows the menu and we will take care of the rest.
      </p>

      <p className="pure-u-1 is-center contact-text">
        Open a new issue at the&nbsp;
        <a href="https://github.com/gjhenrique/BandejaoServer/issues/new">Bandejao repository</a>&nbsp;
        <span className="fa fa-github" />
      </p>
    </div>
  </section>
);

