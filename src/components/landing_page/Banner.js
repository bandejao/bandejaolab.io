import React from 'react';

import device from 'images/device.png';

export default () => (
  <div className="splash-container">
    <div className="splash container pure-g">
      <div className="splash-title pure-u-md-1-2 pure-u-1">
        <h1 className="splash-head">Bring mobility to your university cafeteria</h1>
        <h3 className="splash-subhead">Weekly menu always updated</h3>
      </div>
      <div className="screenshot pure-u-md-1-2 pure-u-1">
        <img src={device} alt="Device" />
      </div>
    </div>
  </div>
);
