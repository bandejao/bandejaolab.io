import React from 'react';

import robot from 'images/robot.svg';
import meals from 'images/meals.svg';
import broadcast from 'images/broadcast.svg';

export default () => (
  <section className="content container pure-g">
    <h2 className="pure-u-1 is-center section-title">
      <i className="fa fa-cog" />
      How it works
    </h2>
    <div className="description l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-3">
      <img src={robot} alt="Robot" />
      <p><strong>1.</strong>Periodically, our robots visit the cafeteria website</p>
    </div>

    <div className="description l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-3">
      <img src={meals} alt="Meals" />
      <p><strong>2.</strong> If the weekly menu changed...</p>
    </div>

    <div className="description l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-3">
      <img src={broadcast} alt="Broadcast" />
      <p><strong className="number">3.</strong> We notify the students with the new meals</p>
    </div>
  </section>
);
