import React from 'react';

export default () => (
  <section className="content container pure-g">
    <h2 className="pure-u-1 is-center section-title"><i className="fa fa-star" />Features</h2>
    <div className="feature l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-3">
      <i className="fa fa-thumbs-o-up" />
      <h3 className="feature-subhead">Useful</h3>
      <p>
        Don&apos;t waste your time going to the ugly cafeteria website to know today&apos;s menu
      </p>
    </div>
    <div className="feature l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-3">
      <i className="fa fa-bullhorn" />
      <h3 className="feature-subhead">Up-to-date</h3>
      <p>
        You are always notified with the latest menu
      </p>
    </div>
    <div className="feature l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-3">
      <i className="fa fa-money" />
      <h3 className="feature-subhead">Free</h3>
      <p>
        No ads and no premium version.
      </p>
    </div>
  </section>
);
