import React from 'react';


import Banner from 'components/landing_page/Banner';
import Feature from 'components/landing_page/Features';
import Methods from 'components/landing_page/Methods';
import Contact from 'components/landing_page/Contact';

export default () => (
  <div>
    <Banner />
    <Feature />
    <Methods />
    <Contact />
  </div>
);
