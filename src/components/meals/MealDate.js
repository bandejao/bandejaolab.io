import React from 'react';

export default ({ date }) => (
  <h2>{ date }</h2>
);
