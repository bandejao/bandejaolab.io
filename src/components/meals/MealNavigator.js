import React from 'react';

export default props => (
  <span>
    {
      props.moveBackward &&
      <a
        role="link"
        tabIndex={0}
        onClick={() => props.decrementDate(props.selectedDate, props.meals)}
        onKeyPress={() => props.decrementDate(props.selectedDate, props.meals)}
      >
        <i className="fa fa-angle-left" />
      </a>
    }
    {props.children}
    {
      props.moveForward &&
      <a
        role="link"
        tabIndex={-1}
        onClick={() => props.incrementDate(props.selectedDate, props.meals)}
        disabled={!props.moveForward}
        onKeyPress={() => props.incrementDate(props.selectedDate, props.meals)}
      >
        <i className="fa fa-angle-right" href="#" />
      </a>
    }
  </span>
);
