import React from 'react';

export default ({ university }) => (
  <h1>
    { university.long_name }
  </h1>
);
