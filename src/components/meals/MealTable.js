import React from 'react';

import MealNavigator from 'components/meals/MealNavigator';

const Period = ({ period }) => (
  <th>{ period === 'BOTH' ? 'LUNCH & DINNER' : period }</th>
);

// Transpose with the  [[A, B, C], [D, E]] into [[A, D], [B, E], [C, undefined]]
const transposeMax = (meals) => {
  const dishes = meals.map(m => m.dishes);
  const max = Math.max(...dishes.map(d => d.length));
  const range = [...Array(max).keys()];
  return range.map(i => dishes.map(d => d[i]));
};

const Dishes = ({ meals }) => (
  transposeMax(meals).map(d => (
    <tr>
      { d.map(x => <td>{x}</td>) }
    </tr>
  ))
);

export default props => (
  <div className="meals-table">
    <MealNavigator {...props} >
      <div className="responsive-table">
        <table className="pure-table pure-table-bordered">
          <thead className="is-center">
            { props.selectedMeals.map(m => <Period key={m.period} period={m.period} />) }
          </thead>
          <tbody>
            <Dishes meals={props.selectedMeals} />
          </tbody>
        </table>
      </div>
    </MealNavigator>
  </div>
);
