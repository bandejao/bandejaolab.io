import React from 'react';

import MealNavigator from 'components/meals/MealNavigator';

export default props => (
  <div className="no-meals">
    <MealNavigator {...props}>
      <h3>No meals for this day</h3>
    </MealNavigator>
  </div>
);
