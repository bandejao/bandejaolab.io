import React from 'react';

export default ({ university }) => (
  <div className="links">
    <a href={university.website} target="_blank">
      <i className="fa fa-globe" />
    </a>
  </div>
);
