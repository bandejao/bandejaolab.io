import React from 'react';

export default ({ toggleMenu, active }) => (
  <button
    className={`hamburger hamburger--collapse ${active ? 'is-active' : ''}`}
    onClick={toggleMenu}
    id="menu-button"
    type="button"
  >
    <span className="hamburger-box">
      <span className="hamburger-inner" />
    </span>
  </button>
);
