import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const UniversityList = ({ universities }) => (
  universities.map(university => (
    <li key={university.name} className="pure-menu-item">
      <Link to={`/daily/${university.name}`}>
        { university.name }
      </Link>
    </li>
  ))
);

UniversityList.defaultProps = {
  universities: [],
};

UniversityList.propTypes = {
  universities: PropTypes.arrayOf(PropTypes.object),
};

export default UniversityList;
