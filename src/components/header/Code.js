import React from 'react';

import CodeRepo from 'components/header/CodeRepo';

export default () => (
  <li className="pure-menu-item">
    <a className="pure-menu-link">
      <span>Code</span>
      <i className="fa fa-chevron-down" />
    </a>
    <ul className="pure-menu-list">
      <CodeRepo name="Android" link="https://github.com/pedro-stanaka/cardapio-ru-uel" />
      <CodeRepo name="Server" link="https://github.com/gjhenrique/EasyMenuServer" />
      <CodeRepo name="React" link="https://github.com/gjhenrique/BandejaoReact" />
    </ul>
  </li>
);
