import React from 'react';

export default ({ link, name }) => (
  <li className="pure-menu-item">
    <a className="pure-menu-link" target="_blank" href={link}>
      {name}
    </a>
  </li>
);
