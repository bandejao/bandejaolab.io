import React from 'react';

import UniversityContainer from 'containers/University';

export default () => (
  <li className="pure-menu-item">
    <a className="pure-menu-link">
      <span>Universities</span>
      <i className="fa fa-chevron-down" />
    </a>

    <ul className="pure-menu-list">
      <UniversityContainer />
    </ul>
  </li>
);
