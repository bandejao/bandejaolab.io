import React from 'react';

import Logo from 'images/logo.png';

export default () => (
  <a className="pure-menu-heading" href="/">
    <img id="logo" alt="Logo" src={Logo} />
    <span className="heading-title">Bandejao</span>
  </a>
);
