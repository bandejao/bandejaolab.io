import React from 'react';

import Home from 'components/header/Home';
import UniversitiesMenu from 'components/header/UniversitiesMenu';
import Code from 'components/header/Code';

export default ({ active }) => (
  <nav id="main-nav" className={`${active ? 'is-active' : ''}`}>
    <ul id="navigation-list" className="pure-menu-list">
      <Home />
      <UniversitiesMenu />
      <Code />
    </ul>
  </nav>
);
