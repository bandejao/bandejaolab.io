import React from 'react';

export default () => (
  <li className="pure-menu-item">
    <a href="/" className="pure-menu-link">
      Home
    </a>
  </li>
);

