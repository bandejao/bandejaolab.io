import {
  FETCH_UNIVERSITIES,
  SUCCESS_UNIVERSITIES,
  FAIL_UNIVERSITIES,
} from '../actions/university_list';

export default (state = {
  loading: false,
  universities: [],
  error: false,
}, action) => {
  switch (action.type) {
    case FETCH_UNIVERSITIES:
      return {
        ...state,
        loading: true,
      };
    case SUCCESS_UNIVERSITIES:
      return {
        ...state,
        universities: action.payload.data,
        error: false,
        loading: false,
      };
    case FAIL_UNIVERSITIES:
      return {
        ...state,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};
