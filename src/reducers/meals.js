import {
  FETCH_MEALS,
  SUCCESS_MEALS,
  FAIL_MEALS,
  SELECT_MEAL,
  fromDateToString,
} from '../actions/meal_fetcher';

export default (state = {
  loading: false,
  meals: [],
  error: false,
  mealNotFound: true,
  selectedDate: fromDateToString(),
}, action) => {
  switch (action.type) {
    case FETCH_MEALS:
      return {
        ...state,
        loading: true,
      };
    case SUCCESS_MEALS:
      return {
        ...state,
        meals: action.payload.data,
        error: false,
        loading: false,
      };
    case FAIL_MEALS:
      return {
        ...state,
        error: true,
        loading: false,
      };
    case SELECT_MEAL:
      return {
        ...state,
        selectedMeals: action.selectedMeals,
        selectedDate: action.selectedDate,
        mealNotFound: action.notFound,
        moveForward: action.moveForward,
        moveBackward: action.moveBackward,
      };
    default:
      return state;
  }
};
