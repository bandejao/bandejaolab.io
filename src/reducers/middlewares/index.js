import { applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import axiosMiddleware from 'redux-axios-middleware';
import { routerMiddleware } from 'react-router-redux';

import api from 'apis';
import history from 'actions/history';

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ ? window.devToolsExtension() : f => f;
const middlewares = applyMiddleware(routerMiddleware(history), thunkMiddleware, axiosMiddleware(api));

export default compose(middlewares, devTools);
