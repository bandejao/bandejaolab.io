import { TOGGLE_MENU } from 'actions/mobile_menu';

export default (state = {
  active: false,
}, action) => {
  if (action.type === TOGGLE_MENU) {
    return { active: !state.active };
  }

  return state;
};
