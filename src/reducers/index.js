import { createStore, combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import universityReducer from 'reducers/university_list';
import mobileReducer from 'reducers/mobile_menu';
import mealsReducer from 'reducers/meals';

import middlewares from 'reducers/middlewares';

export default createStore(
  combineReducers({
    router: routerReducer,
    universities: universityReducer,
    mobile_menu: mobileReducer,
    meals: mealsReducer,
  }),
  middlewares,
);

