import reducer from './university_list';

import {
  FETCH_UNIVERSITIES,
  SUCCESS_UNIVERSITIES,
  FAIL_UNIVERSITIES,
} from '../actions/university_list';

const initialState = {
  error: false,
  loading: false,
  universities: [],
};

describe('reducer', () => {
  it('returns initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('fetches data', () => {
    expect(reducer(initialState, { type: FETCH_UNIVERSITIES }))
      .toEqual({ ...initialState, loading: true });
  });

  it('pass along data from api', () => {
    const payload = {
      data: [{ name: 'PU' }],
    };

    expect(reducer(initialState, { type: SUCCESS_UNIVERSITIES, payload }))
      .toEqual({ ...initialState, loading: false, universities: payload.data });
  });

  it('triggers error flag when api returns error', () => {
    expect(reducer(initialState, { type: FAIL_UNIVERSITIES }))
      .toEqual({ ...initialState, error: true, loading: false });
  });
});
