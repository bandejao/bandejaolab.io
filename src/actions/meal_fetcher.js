import moment from 'moment';

import { mealsRoute } from 'apis/bandejao';

export const FETCH_MEALS = 'meals/FETCH_MEALS';
export const SUCCESS_MEALS = `${FETCH_MEALS}_SUCCESS`;
export const FAIL_MEALS = `${FETCH_MEALS}_FAIL`;
export const SELECT_MEAL = 'meals/SELECT_MEAL';

const today = moment();

export const fetchMealsAction = universityName => ({
  type: FETCH_MEALS,
  payload: {
    request: {
      url: mealsRoute(universityName),
    },
  },
});

export const fromDateToString = (date = today) => (
  moment(date).format('YYYY-MM-DD')
);

const sameWeek = (date, number) => {
  const newDate = moment(date).add(number, 'days');
  return today.isSame(newDate, 'week');
};

export const filterMealsAction = (meals, date = today) => (
  (dispatch) => {
    const moveForward = sameWeek(date, +1);
    const moveBackward = sameWeek(date, -1);

    const selectedDate = fromDateToString(date);

    const selectedMeals = meals.filter(meal => (
      meal.date === selectedDate
    ));

    dispatch({
      type: SELECT_MEAL,
      selectedMeals,
      selectedDate,
      notFound: selectedMeals.length === 0,
      moveForward,
      moveBackward,
    });
  }
);

const changeDate = number => (
  (dateString, meals) => (dispatch) => {
    const newDate = moment(dateString).utc().add(number, 'days');
    dispatch(filterMealsAction(meals, newDate));
  }
);

export const incrementDate = changeDate(1);
export const decrementDate = changeDate(-1);
