import { universitiesRoute } from 'apis/bandejao';

export const FETCH_UNIVERSITIES = 'universities/FETCH_UNIVERSITIES';
export const SUCCESS_UNIVERSITIES = `${FETCH_UNIVERSITIES}_SUCCESS`;
export const FAIL_UNIVERSITIES = `${FETCH_UNIVERSITIES}_FAIL`;

export const fetchUniversity = () => ({
  type: FETCH_UNIVERSITIES,
  payload: {
    request: {
      url: universitiesRoute(),
    },
  },
});
