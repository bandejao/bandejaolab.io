export const TOGGLE_MENU = 'menu/TOGGLE_MENU';

export const toggleMenuAction = () => ({
  type: TOGGLE_MENU,
});
