import React from 'react';
import { connect } from 'react-redux';

import Title from 'components/meals/Title';
import Links from 'components/meals/Links';
import MealDate from 'components/meals/MealDate';
import MealNotFound from 'components/meals/NoMeals';
import MealTable from 'components/meals/MealTable';

import {
  fetchMealsAction,
  filterMealsAction,
  incrementDate,
  decrementDate,
} from 'actions/meal_fetcher';

class MealDetails extends React.Component {
  componentDidMount() {
    this.loadMeals(this.getUniversityName(this.props));
  }

  componentWillReceiveProps(nextProps) {
    const newName = this.getUniversityName(nextProps);
    const oldName = this.getUniversityName(this.props);
    if (oldName !== newName) {
      this.loadMeals(newName);
    }
  }

  getUniversityName(props) {
    const { match: { params: { universityName: newName } } } = props;
    return newName;
  }

  getUniversity(universities) {
    return universities.find(un => (
      un.name === this.getUniversityName(this.props)
    ));
  }

  loadMeals(universityName) {
    const { loadMeals } = this.props;
    loadMeals(universityName);
  }

  mealBody() {
    return this.props.mealNotFound ?
      <MealNotFound {...this.props} /> :
      <MealTable {...this.props} />;
  }

  render() {
    const university = this.getUniversity(this.props.universities);

    return (
      <section className="container meals is-center">
        {
          university &&
          <div>
            <Title university={university} />
            <Links university={university} />
          </div>
        }
        <MealDate date={this.props.selectedDate} />
        { this.mealBody() }
      </section>
    );
  }
}

const mapStateToProps = ({
  meals,
  universities: {
    universities,
  },
}) => ({
  ...meals, universities,
});

const mapDispatchToProps = dispatch => ({
  incrementDate(date, meals) {
    dispatch(incrementDate(date, meals));
  },

  decrementDate(date, meals) {
    dispatch(decrementDate(date, meals));
  },

  loadMeals(universityName) {
    dispatch(fetchMealsAction(universityName))
      .then(response => dispatch(filterMealsAction(response.payload.data)));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MealDetails);
