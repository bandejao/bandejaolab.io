import React from 'react';
import { Route } from 'react-router';

import Footer from 'components/Footer';
import Header from 'containers/Header';
import LandingPage from 'components/landing_page';
import MealDetails from 'containers/MealDetails';

import 'stylesheets';

export default () => (
  <div className="content-wrapper">
    <Header />
    <div className="main-content">
      <Route exact path="/" component={LandingPage} />
      <Route path="/daily/:universityName" component={MealDetails} />
    </div>
    <Footer />
  </div>
);
