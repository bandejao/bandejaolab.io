import React from 'react';
import { connect } from 'react-redux';

import Logo from 'components/header/Logo';
import Menu from 'components/header/Menu';
import MobileMenu from 'components/header/MobileMenu';

import { toggleMenuAction } from 'actions/mobile_menu';

const Header = props => (
  <header className="pure-menu pure-menu-horizontal pure-menu-fixed">
    <div className="container home-menu">
      <Logo />
      <MobileMenu {...props} />
      <Menu {...props} />
    </div>
  </header>
);

const mapStateToProps = ({
  mobile_menu: {
    active,
  },
}) => ({ active });

const mapDispatchToProps = dispatch => ({
  toggleMenu() {
    dispatch(toggleMenuAction());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
