import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { fetchUniversity } from 'actions/university_list';
import UniversityList from 'components/header/UniversityList';
import ErrorUniversity from 'components/header/ErrorUniversity';
import Spinner from 'components/Spinner';

class Container extends Component {
  componentDidMount() {
    this.props.loadUniversities();
  }

  render() {
    if (this.props.loading) {
      return <Spinner />;
    } else if (this.props.error) {
      return <ErrorUniversity />;
    }

    return <UniversityList universities={this.props.universities} />;
  }
}

Container.defaultProps = {
  loadUniversities: f => f,
  universities: [],
  error: false,
  loading: false,
};

Container.propTypes = {
  loadUniversities: PropTypes.func,
  universities: PropTypes.arrayOf(PropTypes.object),
  error: PropTypes.bool,
  loading: PropTypes.bool,
};

const mapStateToProps = ({
  universities: {
    universities,
    error,
    loading,
  },
}) => ({ universities, error, loading });

const mapDispatchToProps = dispatch => ({
  loadUniversities() {
    dispatch(fetchUniversity());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Container);
