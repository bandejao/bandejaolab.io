export const universitiesRoute = () => '/weekly/university/all';

export const mealsRoute = universityName => `/weekly/meals/${universityName}`;
