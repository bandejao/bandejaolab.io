import axios from 'axios';

const client = axios.create({
  baseURL: 'http://localhost:9292',
});

export default client;

